<?php

/**
 * KL_AnonymousLinks_Listener_BbCode
 *
 *	@author: Katsulynx
 *  @last_edit:	15.06.2016
 */

class KL_AnonymousLinks_Listener_BbCode {
    public static function extend($class, array &$extend) {
        $extend[] = 'KL_AnonymousLinks_BbCode_Formatter_URL';
    }
}