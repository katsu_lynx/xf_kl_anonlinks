<?php

class KL_AnonLinks_BbCode_Formatter_URL extends XFCP_KL_AnonLinks_BbCode_Formatter_URL {
    public function renderTagUrl(array $tag, array $rendererStates) {
        $options = XenForo_Application::get('options');

		$parent = parent::renderTagUrl($tag, $rendererStates);

        preg_match('/href="(.*)"/sU',$parent,$url);

        $url_domain = parse_url($url[1], PHP_URL_HOST);

        $whitelist = $options->kl_anonlinks_whitelist."\n".parse_url($options->boardUrl, PHP_URL_HOST);
        $whitelist = str_replace('\*','.*',str_replace("\n",'|',preg_quote($whitelist)));

        if(!preg_match('/'.$whitelist.'/', $url_domain)) {
            $encode = (bool) substr($options->kl_anonlinks_service,0,1);
            $service = substr($options->kl_anonlinks_service,1);

            $parent = str_replace(
                'href="'.$url[1],
                'href="'.$service.($encode ? urlencode($url[1]) : $url[1]),
                $parent
            );
        }

        return $parent;
    }
}