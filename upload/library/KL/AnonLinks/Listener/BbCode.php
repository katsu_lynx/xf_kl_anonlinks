<?php

/**
 * KL_FontsManager_Listener_BBCode
 *
 *	@author: Katsulynx
 *  @last_edit:	17.09.2015
 */

class KL_AnonLinks_Listener_BbCode {
    public static function extend($class, array &$extend) {
        $extend[] = 'KL_AnonLinks_BbCode_Formatter_URL';
    }
}